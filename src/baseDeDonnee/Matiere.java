package baseDeDonnee;

/**
 * Enumeration qui represente une matiere enseignee.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public enum Matiere {
	MATHEMATIQUES,
	PHYSIQUE,
	CHIMIE,
	BIOLOGIE,
	FRANCAIS,
	ANGLAIS,
	ESPAGNOL,
	ALLEMAND,
	HISTOIRE,
	GEOGRAPHIE;
}

