package baseDeDonnee;

import baseDeDonnee.Cours;
import baseDeDonnee.Matiere;
import baseDeDonnee.Professeur;
import baseDeDonnee.Promotion;
import baseDeDonnee.Salle;

/**
 * Classe representant un cours.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Cours {
	private Salle salle;
	private Professeur prof;
	private Promotion promo;
	private Matiere matiere;	
	
	/** 
	 * constructeur de cours
	 * @param salle
	 * @param prof
	 * @param promo
	 * @param matiere
	 */
	public Cours(Salle salle, Professeur prof, Promotion promo,
			Matiere matiere) {
		this.salle = salle;
		this.prof = prof;
		this.promo = promo;
		this.matiere = matiere;	
	}

	/**
	 * Constructeur de Cours
	 */
	public Cours() {
		super();
		this.salle=new Salle();
		this.prof=new Professeur();
		this.promo=new Promotion();
		this.matiere=null;
	}


	/**
	 * getter de la salle
	 * @return
	 */
	public Salle getSalle() {
		return this.salle;
	}

	/**
	 * getter du prof
	 * @return
	 */
	public Professeur getProf() {
		return this.prof;
	}

	/**
	 * getter de la promo
	 * @return
	 */
	public Promotion getPromo() {
		return this.promo;
	}

	/**
	 * getter de la matiere
	 */
	public Matiere getMatiere() {
		return this.matiere;
	}


	/**
	 * setter de la salle
	 * @param salle
	 */
	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	/**
	 * setter du prof
	 * @param prof
	 */
	public void setProf(Professeur prof) {
		this.prof = prof;
	}

	/**
	 * setter de la promo
	 * @param promo
	 */
	public void setPromo(Promotion promo) {
		this.promo = promo;
	}


	/**
	 * setter de la matiere
	 * @param matiere
	 */
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	
	public boolean equals(Cours c ) {
		if (c.getSalle() == this.salle && c.getMatiere()==this.matiere && 
				c.getPromo()==this.promo && c.getProf()==this.prof) {
			return true;
		}
		return false;
	}


}