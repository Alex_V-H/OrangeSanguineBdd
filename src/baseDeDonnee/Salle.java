package baseDeDonnee;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Classe representant une salle.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Salle implements IDisponible{
	private int nbPlaces;
	private String nom;
	private Map<Equipement,Integer> equipements;
	private Calendrier calendrier;
	
	/** 
	 * constructeur de  une salle
	 * @param nbPlaces
	 * @param nom
	 */
	public Salle(int nbPlaces, String nom) {
		this.nbPlaces = nbPlaces;
		this.nom = nom;
		this.equipements=new HashMap<Equipement,Integer>();
		this.calendrier =new Calendrier();
	}
	
	
	/** 
	 * constructeur de  une salle
	 * @param nbPlaces
	 * @param nom
	 * @param equipements
	 */
	public Salle(int nbPlaces, String nom, Map<Equipement, Integer> equipements) {
		this.nbPlaces = nbPlaces;
		this.nom = nom;
		this.equipements = equipements;
		this.calendrier =new Calendrier();
	}
	
	/**
	 * Constructeur de salle
	 */
	public Salle() {
		super();
	}


	/** 
	 * getter du nombre de places
	 * @return
	 */
	public int getNbPlaces() {
		return nbPlaces;
	}
	
	public void setCalendrier(Calendrier calendrier) {
		this.calendrier = calendrier;
	}


	/**
	 * getter du nom de la salle
	 * @return
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * getter des equipements de la salle
	 * @return
	 */
	public Map<Equipement, Integer> getEquipements() {
		return equipements;
	}
	
	/**
	 * getteur du calendrier de la salle
	 * @return
	 */
	public Calendrier getCalendrier() {
		return this.calendrier;
	}


	/**
	 * setter du nombre de place
	 * @param nbPlaces
	 */
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	/**
	 * setter du nom
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * setter des equipements
	 * @param equipements
	 */
	public void setEquipements(Map<Equipement, Integer> equipements) {
		this.equipements = equipements;
	}

	@Override
	public String toString() {
		return "nom : " + this.getNom() + "\n" 
				+ "nombre de place : " + this.getNbPlaces() + "\n"
				+ "equipements : " + this.getEquipements();
	}
	
	
	@Override
	public List<Creneau> disponible(Calendar calendar) {
		List<Creneau> c =this.calendrier.getEmploiDuTemps();
		List<Creneau> l = new ArrayList<Creneau>();
		int j;
		for (j =0; j< c.size();j++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(c.get(j).getDateDebut().getTime());
			if (c.get(j).getCours()==null) {
				l.add(c.get(j));
			}
		}
	return l;
	}
}
