package baseDeDonnee;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe representant un calendrier.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Calendrier {
	
	private List<Creneau> emploiDuTemps;
	
	public Calendrier() {
		this.emploiDuTemps = new ArrayList<Creneau>();
	}

	public Calendrier(List<Creneau> emploiDuTemps) {
		this.emploiDuTemps = emploiDuTemps;
	}

	public List<Creneau> getEmploiDuTemps() {
		return this.emploiDuTemps;
	}

	public void setEmploiDuTemps(List<Creneau> emploiDuTemps) {
		this.emploiDuTemps = emploiDuTemps;
	}
	
	
	

}
