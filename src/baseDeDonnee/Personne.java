package baseDeDonnee;

/**
 * Classe representant une personne.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public abstract class Personne {
	private String nom;
	private String prenom;

	
	/**
	 * constructeur de personne
	 * @param nom
	 * @param prenom
	 */
	public Personne(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}
	
	/**
	 * Constructeur de personne
	 */
	public Personne() {
		super();
	}
	/**
	 * getter de nom
	 * @return
	 */
	public String getNom() {
		return this.nom;
	}
	


	/**
	 * getter de prenom
	 * @return
	 */
	public String getPrenom() {
		return this.prenom;
	}
	
	/**
	 * setter de nom
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * setter de prenom
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	@Override
	public String toString() {
		return "Nom : " +this.getNom() + "\nPrenom : " + this.getPrenom();
	}
}
