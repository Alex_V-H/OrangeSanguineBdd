package baseDeDonnee;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Classe representant une promotion.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Promotion implements IDisponible{
	private List<Etudiant> etudiants;
	private Calendrier calendrier;
	private String id;


	public Promotion() {
		this.etudiants = new ArrayList<Etudiant>();
		this.calendrier = new Calendrier();
		this.id="";
	}


	/**
	 * constructeur d une promotion
	 * @param etudiants
	 * @param calendrier
	 */
	public Promotion(List<Etudiant> etudiants, String id) {
		this.etudiants = etudiants;
		this.id=id;
		this.calendrier = new Calendrier();
	} 


	/**
	 * constructeur d une promotion
	 * @param etudiants
	 * @param calendrier
	 */
	public Promotion(List<Etudiant> etudiants, Calendrier calendrier, String id) {
		this.etudiants = etudiants;
		this.calendrier = calendrier;
	} 

	/**
	 * getter de la liste d etudiant de la promo
	 * @return
	 */
	public List<Etudiant> getEtudiants() {
		return etudiants;
	}


	/**
	 * getter du calendrier de la promo
	 * @return
	 */
	public Calendrier getCalendrier() {
		return calendrier;
	}

	/**
	 *getter de l id de la promo 
	 * @return
	 */
	public String getId() {
		return this.id;
	}


	/**
	 * modification d un eleve dans la liste d etudiant
	 * l aoute s il n y est pas, le modifie sinon
	 * @param etudiants
	 */
	public void setEtudiants(Etudiant etudiant) {
		if (this.etudiants.contains(etudiant)) {
		}
	}

	/**
	 * setter du calendrier de la promotion
	 * @param calendrier
	 */
	public void setCalendrier(Calendrier calendrier) {
		this.calendrier = calendrier;
	}

	/**
	 * calcul le nombre d eleves dans la classe
	 * @return
	 */
	public int calculEffectif() {
		return this.etudiants.size();
	}

	@Override
	public List<Creneau> disponible(Calendar calendar) {
		List<Creneau> c =this.calendrier.getEmploiDuTemps();
		List<Creneau> l = new ArrayList<Creneau>();
		int j;
		for (j =0; j< c.size();j++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(c.get(j).getDateDebut().getTime());
			if (c.get(j).getCours()==null) {
				l.add(c.get(j));
			}
		}
	return l;
	}

}
