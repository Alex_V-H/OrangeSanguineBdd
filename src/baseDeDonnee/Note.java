package baseDeDonnee;

import classe.Etudiant;
import classe.Matiere;

/**
 * Classe representant une note.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Note {
	private Etudiant etudiant;
	private Matiere matiere;
	private double valeur;
	private int coeff;
	
	/**
	 * constructeur de note
	 * @param etudiant
	 * @param matiere
	 * @param note
	 * @param coeff
	 */
	public Note(Etudiant etudiant, Matiere matiere, double note, int coeff) {
		this.etudiant = etudiant;
		this.matiere = matiere;
		this.valeur = note;
		this.coeff = coeff;
	}

	/**
	 * getter de l etudiant
	 * @return
	 */
	public Etudiant getEtudiant() {
		return etudiant;
	}
	
	/** 
	 * getter de la matiere
	 * @return
	 */
	public Matiere getMatiere() {
		return matiere;
	}
	
	/**
	 * getter de la valeur de la note
	 * @return
	 */
	public double getValeur() {
		return valeur;
	}

	/**
	 * getter du coeff de la note
	 * @return
	 */
	public int getCoeff() {
		return coeff;
	}

	/**
	 * setter de l etudiant
	 * @param etudiant
	 */
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	
	/**
	 * setter de la matiere
	 * @param matiere
	 */
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	/**
	 * setter de la valeur de la note
	 * @param valeur
	 */
	public void setValeur(double valeur) {
		this.valeur = valeur;
	}
	
	
	/**
	 * setter du coeff de la note
	 * @param coeff
	 */
	public void setCoeff(int coeff) {
		this.coeff = coeff;
	}
	
	

}