package baseDeDonnee;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Classe representant un professeur.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Professeur extends Personne implements IDisponible{
	private Matiere matiere;
	private Calendrier calendrier;


	/**
	 * constructeurs de professeur
	 */
	public Professeur(String nom, String prenom, Matiere matiere) {
		super(nom, prenom);
		this.matiere = matiere;
		this.calendrier = new Calendrier();
	}

	/**
	 * Constructeur de Professeur
	 */
	public Professeur() {
		super();
		this.matiere=null;
		this.calendrier=new Calendrier();
	}


	/**
	 * constructeur de professeur
	 */
	public Professeur(String nom, String prenom, Matiere matiere, Calendrier calendrier) {
		super(nom, prenom);
		this.matiere = matiere;
		this.calendrier = calendrier;
	}

	/**
	 * getter matiere
	 * @return
	 */
	public Matiere getMatiere() {
		return matiere;
	}

	/**
	 * getter calendrier
	 * @return
	 */
	public Calendrier getCalendrier() {
		return calendrier;
	}

	/**
	 * setter matiere
	 * @param matiere
	 */
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}


	/**
	 * setter calendrier
	 * @param calendrier
	 */
	public void setCalendrier(Calendrier calendrier) {
		this.calendrier = calendrier;
	}


	/**
	 * methode d ajout d une note apr un professeur dans les notes d un eleve
	 * @param etudiant
	 * @param note
	 */
	public void noter(Note note) {
		Map<Matiere,List<Note>> aux = note.getEtudiant().getNotes();

		Set<Matiere> cles = note.getEtudiant().getNotes().keySet();
		Iterator<Matiere> it = cles.iterator();
		while(it.hasNext()) {
			Matiere cle = it.next();
			if (cle == this.matiere){
				List<Note> l = note.getEtudiant().getNotes().get(cle);
				l.add(note);
				aux.put(this.matiere,l);
				note.getEtudiant().setNotes(aux);
				return;
			}
		}
		List<Note> l =new ArrayList<Note>();
		l.add(note);
		aux.put(this.matiere, l);
		note.getEtudiant().setNotes(aux);
	}

	@Override
	public String toString() {
		return super.toString() + "\n"  
				+ "matiere : " + this.getMatiere();
	}


	@Override
	public List<Creneau> disponible(Calendar calendar) {
		List<Creneau> c =this.calendrier.getEmploiDuTemps();
		List<Creneau> l = new ArrayList<Creneau>();
		int j;
		for (j =0; j< c.size();j++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(c.get(j).getDateDebut().getTime());
			if (c.get(j).getCours()==null) {
				l.add(c.get(j));
			}
		}
		return l;
	}
}
