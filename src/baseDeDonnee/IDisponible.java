package baseDeDonnee;

import java.util.Calendar;
import java.util.List;

/**
 * Interface utiliser pour connaitre la disponibilite d une instance.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public interface IDisponible {
	public abstract List<Creneau> disponible(Calendar calendar);
}
