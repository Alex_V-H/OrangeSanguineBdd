package baseDeDonnee;

import java.sql.Connection;
import java.sql.DriverManager;

public class Connexion {

	private final String DB_URL = "jdbc:postgresql://localhost:5432/OrangeSanguineBDD";
	private final String USER = "postgres";

	private final String PASS = "postgres";
	private static Connection conn; // l’objet Connection



	private Connexion(){ // Constructeur prive ! Objet impossible a instancier
		try{
			conn = DriverManager.getConnection(DB_URL, USER, PASS);;


		}


		catch(Exception e){
			e.printStackTrace();

		}

	}

	// methode qui retourne l’instance et qui la cree si elle n’existe pas encore
	public static Connection getInstance(){

		if(conn == null){


			new Connexion();
		}



		return conn;
	}
}