package baseDeDonnee;

import java.util.ArrayList;
import java.util.List;

import baseDeDonnee.Etudiant;
import baseDeDonnee.Matiere;
import baseDeDonnee.Professeur;
import baseDeDonnee.Promotion;

public class Main {

	public static void main(String[] args) {
		Professeur prof1 = new Professeur("Costes","Benoit", Matiere.MATHEMATIQUES);
		Professeur prof2 = new Professeur("Hangouet","Jean-Francois", Matiere.FRANCAIS);
		Professeur prof3 = new Professeur("Ennafi","Oussama", Matiere.CHIMIE);
		Professeur prof4 = new Professeur("Pelardi","Patricia", Matiere.HISTOIRE);
		Professeur prof5 = new Professeur("Coindet","Victor", Matiere.BIOLOGIE);


		Etudiant etud1 = new Etudiant("Van Hecke","Alex");
		Etudiant etud2 = new Etudiant("Sobreira","Oriane");
		Etudiant etud3 = new Etudiant("Sengeissen","Enola");
		Etudiant etud4 = new Etudiant("Petiet","Thibault");
		Etudiant etud5 = new Etudiant("Chassard","Axel");

		List<Etudiant> etudiants1 = new ArrayList<Etudiant>();
		etudiants1.add(etud1);
		etudiants1.add(etud2);
		etudiants1.add(etud3);
		List<Etudiant> etudiants2 = new ArrayList<Etudiant>();
		etudiants2.add(etud4);
		etudiants2.add(etud5);


		Promotion promo1 = new Promotion(etudiants1,"IT1");
		Promotion promo2 = new Promotion(etudiants2,"IT2");

		etud1.setPromo(promo1);
		etud2.setPromo(promo1);
		etud3.setPromo(promo1);

		etud4.setPromo(promo2);
		etud5.setPromo(promo2);
		
		etud1.editerBulletin();
		
	}

}
