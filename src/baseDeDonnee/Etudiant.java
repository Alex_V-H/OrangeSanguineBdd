package baseDeDonnee;


import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Etudiant extends Personne{
	private Promotion promo;
	private Map<Matiere,List<Note>> notes;


	/**
	 * constructeur etudiant
	 * @param nom
	 * @param prenom
	 */
	public Etudiant(String nom, String prenom) {
		super(nom, prenom);
		this.notes = new HashMap<Matiere,List<Note>>();
	}

	/**
	 * constructeur etudiant
	 * @param nom
	 * @param prenom
	 * @param promo
	 * @param notes
	 */
	public Etudiant(String nom, String prenom, Promotion promo, Map<Matiere, List<Note>> notes) {
		super(nom, prenom);
		this.promo = promo;
		this.notes = notes;
	}

	/**
	 * getter de la promo
	 * @return
	 */
	public Promotion getPromo() {
		return promo;
	}

	/**
	 * getter de la liste de note
	 * @return
	 */
	public Map<Matiere, List<Note>> getNotes() {
		return notes;
	}

	/**
	 * setter de la promo
	 * @param promo
	 */
	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	/**
	 * setter de la liste de note
	 * @param notes
	 */
	public void setNotes(Map<Matiere, List<Note>> notes) {
		this.notes = notes;
	}



	/**
	 * methode permettant d afficher le bulletin de notes
	 */
	public void visualiserBulletin() {

		Connection conn = null;
		try{
			Class.forName("org.postgresql.Driver"); // leve une exception si le pilote Postgresql n’est pas installe
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/OrangeSanguineBDD", "postgres", "postgres"); // ouvre une connexion vers la base de donnees data
			System.out.println("Connexion etablie avec succes !");
		}
		catch(Exception e){
			e.printStackTrace(); // pour gerer les erreurs (pas de pilote, base inexistante, etc.)
		}
		finally{
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} // toujours fermer les differentes ressources quand il n’y en as plus besoin
			}
		}
		try {
			//Creation d’un objet Statement
			Statement state = conn.createStatement();
			//L’objet ResultSet contient le resultat de la requete SQL
			ResultSet result = state.executeQuery("SELECT valeur FROM Note, Etudiant WHERE Note.id_Etudiant = Etudiant.id_Personne AND Etudiant.Nom LIKE "+this.getNom()+
					"AND Etudiant.Prenom LIKE "+ this.getPrenom()); // execution d’une requete SQL
			result.next(); // 1er appel a next : on place le curseur sur la premiere ligne de resultats
			int id = result.getInt("id_Personne"); // on recupere le resultat, premiere ligne, colonne "id"
			//String name = result.getString("name"); // on recupere le resultat, premiere ligne, colonne "name"

			ResultSetMetaData resultMeta = result.getMetaData();
			while(result.next()){ // on va iterativement deplacer le curseur sur les differentes lignes de resultats
				for(int i = 1; i <= resultMeta.getColumnCount(); i++){ // attention ! Les indices commencent a 1 ici !!!
					System.out.print("\t" + result.getObject(i).toString() + "\t |");
					//REPRENDRE IIIIIIICCCCCIIIIII  this.notes.setNote(matiere, note, coeff);
				}
				System.out.println("\n---------------------------------");
			}
			result.close();
			state.close();

			Set<Matiere> cles = this.notes.keySet();
			Iterator<Matiere> it = cles.iterator();

			Map<Matiere,Double> moyMat = calculMoyMatiere();

			System.out.println("Etudiant "+ this.getNom() + " " + this.getPrenom());
			while(it.hasNext()) {
				Matiere cle =it.next();
				System.out.println("Matiere : " + cle);
				for (Note note : this.notes.get(cle)) {
					System.out.println("note : " + note.getValeur()+ "\t" + 
							"coeff : " + note.getCoeff() + "\t");
				}
				System.out.println("Moyenne : " + (double)Math.round(moyMat.get(cle) * 10) / 10);
				System.out.println("\n");
			}
			System.out.println("Moyenne generale : " + (double)Math.round(this.calculMoyGenerale() *10) / 10);
		}catch(Exception e){
			e.printStackTrace(); // pour gerer les erreurs (pas de pilote, base inexistante, etc.)
		}
		finally{
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} // toujours fermer les differentes ressources quand il n’y en as plus besoin
			}
		}
	}


		/**
		 * calcul de la moyenne dans chaque matiere
		 * @return
		 */
		public Map<Matiere,Double> calculMoyMatiere() {
			Map<Matiere,Double> map = new HashMap<Matiere,Double>();

			Set<Matiere> cles = this.notes.keySet();
			Iterator<Matiere> it = cles.iterator();


			while(it.hasNext()) {
				int c=0;
				double somme=0;
				Matiere cle = it.next();
				for(Note note : this.notes.get(cle)) {
					somme=somme + note.getValeur()*note.getCoeff();
					c+=note.getCoeff();

				}
				map.put(cle,somme/c);
			}
			return map;
		}


		/**
		 * calcul de la moyenne generale 
		 * @return
		 */
		public double calculMoyGenerale() {
			Map<Matiere,Double> map = calculMoyMatiere();

			Set<Matiere> cles = map.keySet();
			Iterator<Matiere> it = cles.iterator();
			double somme =0;
			int c = 0;
			while (it.hasNext()) {
				Matiere cle=it.next();
				somme += map.get(cle);
				c+=1;
			}
			return (somme/c);
		}


		/**
		 * 
		 * @param etudiant
		 * @return
		 */
		public boolean compareEtudiant(Etudiant etudiant) {
			return ((this.getPrenom() == etudiant.getPrenom()) &&
					(this.getNom() == etudiant.getNom()));
		}



		public void editerBulletin() {

			Calendar today = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
			String strDate = dateFormat.format(today.getTime());

			String path = "/home/formation/Bulletin" + this.getNom() + ".txt";
			Path p = Paths.get(path);
			if (!Files.exists(p)) {
				try { Files.createFile(p);
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			try (BufferedWriter writer = Files.newBufferedWriter(p, StandardCharsets.UTF_8,
					StandardOpenOption.WRITE)) {


				writer.write("Bulletin de : " + this.getNom() + " " + this.getPrenom()
				+ "\n\nEdité le : "+ strDate +"\n\nMatière \t\tMoyenne\n\n");

				Set<Matiere> cles = this.notes.keySet();
				Iterator<Matiere> it = cles.iterator();
				Map<Matiere,Double> moyMat = calculMoyMatiere();

				while(it.hasNext()) {
					Matiere cle =it.next();
					writer.write(cle + " \t\t" + (double)Math.round(moyMat.get(cle) * 10) / 10 +"\n");         
				}
				writer.write("\nMoyenne générale : " + (double)Math.round(this.calculMoyGenerale() *10) / 10 );            
			}
			catch (IOException e) {
				e.printStackTrace();

			}    
		}

		/**
		 * Methode pour permettre une visualisation dans le terminal de l'emploi du temps de l etudiant.
		 */
		public void visualiserEmploiDuTemps() {
			Calendar today = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
			String strDateToday= dateFormat.format(today.getTime());	
			Calendrier cal = this.promo.getCalendrier();
			int c = 0;
			System.out.println(dateFormat.format(cal.getEmploiDuTemps().get(c).getDateDebut().getTime()));
			System.out.println(strDateToday);
			System.out.println("\n\nEmploi du temps de l'étudiant.e : " + this.getPrenom() + " " + this.getNom() + " (promo : " + this.promo.getId() +")\n");
			while(c<cal.getEmploiDuTemps().size() && dateFormat.format(cal.getEmploiDuTemps().get(c).getDateDebut().getTime()).equals((strDateToday))==false) {
				c++;
			}
			for (int i = c; i<c+70 && i<cal.getEmploiDuTemps().size();i++) {
				String s =""; 
				DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
				String strDate2 = dateFormat2.format(cal.getEmploiDuTemps().get(i).getDateDebut().getTime());
				if (cal.getEmploiDuTemps().get(i).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
					s+="\t" + strDate2.substring(0,10) + "\n";
				}
				s+= strDate2.substring(11) + "\t";
				if (cal.getEmploiDuTemps().get(i).getCours()!=null) {
					s+=cal.getEmploiDuTemps().get(i).getCours().getSalle().getNom()+" , " +
							cal.getEmploiDuTemps().get(i).getCours().getProf().getPrenom() + " " +
							cal.getEmploiDuTemps().get(i).getCours().getProf().getNom();
				}
				else {
					s+="--";
				}
				System.out.println(s);
			}
		}



	}
