package baseDeDonnee;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


/**
 * Classe representant le personnel administratif.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class PersonnelAdministratif extends Personne{

	public PersonnelAdministratif(String nom, String prenom) {
		super(nom, prenom);
	}


	/**
	 * methode permettant de visualiser l emploi du temps d un professeur
	 * @param prof
	 */
	public void visualiserEmploiDuTempsProf(Professeur prof) {
		Calendrier cal = prof.getCalendrier();
		System.out.println("Emploi du temps du prof : " + prof.getPrenom() +" "+
				prof.getNom()+"\n[ "+ prof.getMatiere() + " ]\n");
		Calendar today = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
		String strDateToday= dateFormat.format(today.getTime());
		int c=0;
		while(c<cal.getEmploiDuTemps().size() && dateFormat.format(cal.getEmploiDuTemps().get(c).getDateDebut().getTime()).equals((strDateToday))==false) {
			c++;
		}
		for (int i = c; i<c+70;i++) {
			String s =""; 
			DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
			String strDate = dateFormat2.format(cal.getEmploiDuTemps().get(i).getDateDebut().getTime());
			if (cal.getEmploiDuTemps().get(i).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
				s+="\t" + strDate.substring(0,10) + "\n";
			}
			s+= strDate.substring(11) + "\t";
			if (cal.getEmploiDuTemps().get(i).getCours()!=null) {
				s+= cal.getEmploiDuTemps().get(i).getCours().getSalle().getNom()+" , " +
						cal.getEmploiDuTemps().get(i).getCours().getPromo().getId();
			}
			else {
				s+="--";
			}
			System.out.println(s);
		}
	}



	/**
	 * methode permettant de visualiser l emploi du temps d une promotion
	 * @param promo
	 */
	public void visualiserEmploiDuTempsPromo(Promotion promo) {
		Calendrier cal = promo.getCalendrier();
		System.out.println("Emploi du temps de la promo : " + promo.getId() + "\n");
		Calendar today = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
		String strDateToday= dateFormat.format(today.getTime());
		int c=0;
		while(c<cal.getEmploiDuTemps().size() && dateFormat.format(cal.getEmploiDuTemps().get(c).getDateDebut().getTime()).equals((strDateToday))==false) {
			c++;
		}
		for (int i = c; i<c+70;i++) {
			String s =""; 
			DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
			String strDate = dateFormat2.format(cal.getEmploiDuTemps().get(i).getDateDebut().getTime());
			if (cal.getEmploiDuTemps().get(i).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
				s+="\t" + strDate.substring(0,10) + "\n";
			}
			s+= strDate.substring(11) + "\t";
			
			if (cal.getEmploiDuTemps().get(i).getCours()!=null) {
				s+=cal.getEmploiDuTemps().get(i).getCours().getSalle().getNom()+" , " +
						cal.getEmploiDuTemps().get(i).getCours().getProf().getPrenom() + " " +
						cal.getEmploiDuTemps().get(i).getCours().getProf().getNom();
			}
			else {
				s+="--";
			}
			System.out.println(s);
		}

	}


	/**
	 * methode ayant pour but d ajouter un cours a un prof, etudiant et salle particuliere en fonction de leur dsiponibilite
	 * interraction avec l utilisateur qui choisi le creneau parmis une selection
	 * @param calendar
	 * @param salle
	 * @param prof
	 * @param promo
	 * @param matiere
	 * @param nbCreneaux
	 * @param equipements
	 */
	public void ajouterCours(Calendar calendar, Salle salle, Professeur prof, 
			Promotion promo, Matiere matiere,
			Map<Equipement, Integer> equipements) {
		List<Creneau> listCrenProf = new ArrayList<Creneau>();
		List<Creneau> listCrenPromo = new ArrayList<Creneau>();
		List<Creneau> listCrenSalle = new ArrayList<Creneau>();
		if (salle.getNbPlaces()>= promo.calculEffectif()) {
			if (prof.getMatiere().equals(matiere)) {
				Set<Equipement> cles = equipements.keySet();
				Iterator<Equipement> it = cles.iterator();
				int s = 0;
				while (it.hasNext()) {
					Equipement cle =it.next();
					if (salle.getEquipements().containsKey(cle)) {
						if (equipements.get(cle)==salle.getEquipements().get(cle)) {
							s++;
						}
						else {
							System.out.println("L'equipement : "+ cle + " n'est pas en quantité suffisante.");
						}
					}
					else {
						System.out.println("L'equipement : "+ cle + " n'est pas disponible dans cette salle.");
					}
				}
				if (s==equipements.size()) {
					listCrenProf = prof.disponible(calendar);
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
					listCrenPromo = promo.disponible(calendar);
					listCrenSalle = salle.disponible(calendar);
					List<List<Integer>> listIntersect = new ArrayList<List<Integer>>();
					for ( int i = 0; i<listCrenProf.size();i++) {
						for ( int j = 0; j<listCrenPromo.size();j++) {
							for ( int k = 0; k<listCrenSalle.size();k++) {
								if ((listCrenProf.get(i).getDateDebut()).compareTo(listCrenSalle.get(k).getDateDebut())==0 &&
										(listCrenProf.get(i).getDateDebut()).compareTo(listCrenPromo.get(j).getDateDebut())==0) {
									List<Integer> l = new ArrayList<Integer>();
									l.add(i);
									l.add(j);
									l.add(k);
									listIntersect.add(l);
								}
							}

						}
					}
					System.out.println("\nLes disponibilités pour la semaine à venir sont : \n");
					
					Calendar today = Calendar.getInstance();
					Calendar weekAfter = Calendar.getInstance();
					weekAfter.add(Calendar.DATE,7);
					int c=0;
					while( listCrenProf.get(listIntersect.get(c).get(0)).getDateDebut().compareTo(today)<0) {
						c++;
					}
					String s2 ="";
					for (int i=c;i<listIntersect.size() && listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().compareTo(weekAfter)<0;i++) {
						s2 =""; 
						DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
						String strDate = dateFormat.format(listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().getTime());
						if (listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
							s2+="\t" + strDate.substring(0,10) + "\n" + strDate.substring(11);
						}
						else {
							s2+= strDate.substring(11) + "\t";
						}
						System.out.println(s2);
					}
					Calendar cal = Calendar.getInstance();
					boolean a=true;
					String date2 = "";
					while(a) {
						Scanner sc = new Scanner(System.in);
						System.out.println("Quel creneau choisissez-vous ? (dd-MM-yyyy-HH:mm:ss)");
						date2 = sc.nextLine();
						while (!(date2.length()==19 && date2.substring(2,3).equals("-") && date2.substring(5,6).equals("-") && 
							date2.substring(10,11).equals("-") && date2.substring(13,14).equals(":") && date2.substring(16,17).equals(":"))) {
							System.out.println("\nLe creneau que vous avez saisi n'existe pas.\nVeuillez recommencer:\n");
							sc = new Scanner(System.in);
							System.out.println("Quel creneau choisissez-vous ? (dd-MM-yyyy-HH:mm:ss)");
							date2 = sc.nextLine();
							a = false;
						}
						String[] date = date2.split("-",4);
						String[] horaire = date[3].split(":",3);
						int jour = Integer.parseInt(date[0]);
						int mois = Integer.parseInt(date[1]);
						int annee = Integer.parseInt(date[2]);
						int heure = Integer.parseInt(horaire[0]);
						int minute = Integer.parseInt(horaire[1]);
						int seconde = Integer.parseInt(horaire[2]);
						if (annee>2025 || annee<2019 || mois>12 || mois<0 ||
								jour>31 || jour<0 || heure<8 || heure==13 || 
								heure>18 || minute>=60 || minute<0 || seconde>=60 || seconde<0) {
							System.out.println("\nLe creneau que vous avez saisi n'existe pas.\nVeuillez recommencer:\n");	
							a=true;
						}
						else {
							a=false;
						}

					}
					try {
						cal.setTime(sdf.parse(date2));
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
					Cours cours = new Cours(salle, prof, promo, matiere);
					for (int i=0;i<listIntersect.size();i++) {
						if (listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().compareTo(cal)==0) {
							listCrenProf.get(listIntersect.get(i).get(0)).setCours(cours);
						}
						if (listCrenPromo.get(listIntersect.get(i).get(1)).getDateDebut().compareTo(cal)==0) {
							listCrenPromo.get(listIntersect.get(i).get(1)).setCours(cours);
						}
						if (listCrenSalle.get(listIntersect.get(i).get(2)).getDateDebut().compareTo(cal)==0) {
							listCrenSalle.get(listIntersect.get(i).get(2)).setCours(cours);
						}
					}	    
				}
			}
			else {
				System.out.println("Le prof n enseigne pas cette matiere.");
			}
		}
		else {
			System.out.println("Impossible d'ajouter le cours -- Place insuffisante.");
		}
	}



	/**
	 * methode ayant pour but de supprimer un cours 
	 * @param calendar
	 * @param promo
	 */	
	public void supprimerCours(Calendar calendar,Promotion promo) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
		List<Creneau> cal = promo.getCalendrier().getEmploiDuTemps();
		Professeur prof = new Professeur();
		Salle salle =new Salle();
		Cours c = new Cours();
		for (int i = 0; i<cal.size();i++) {
			if (cal.get(i).getDateDebut().compareTo(calendar)==0) {
				c = cal.get(i).getCours();
				if (c!=null) {
					prof = c.getProf();
					salle = c.getSalle();
					prof.getCalendrier().getEmploiDuTemps().get(i).setCours(null);
					salle.getCalendrier().getEmploiDuTemps().get(i).setCours(null);
					cal.get(i).setCours(null);
					break;
				}
				else {

					System.out.println("Pas de cours a supprimer a la promo " + promo.getId() + " au creneau du " + sdf.format(calendar.getTime()));
					return;
				}
			}
			if (cal.get(i).getDateDebut().compareTo(calendar)>0) {
				System.out.println("Date " + sdf.format(calendar.getTime()) +  " incorrecte, pas de correspondance avec le calendrier de la promo " + promo.getId());
				break;
			}
		}
	}


}
