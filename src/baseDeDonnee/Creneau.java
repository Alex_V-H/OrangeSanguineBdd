package baseDeDonnee;

import java.util.Calendar;

import classe.Cours;


/**
 * Classe representant un creneau.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Creneau {
	private Calendar calendar;
	private Cours cours;
	
	
	/**
	 * conqstructeur de creneau
	 * @param dateDebut
	 * @param cours
	 */
	public Creneau(Calendar calendar) {
		this.calendar = calendar;
		this.cours = null;
	}
	
	/**
	 * conqstructeur de creneau
	 * @param dateDebut
	 * @param cours
	 */
	public Creneau(Calendar calendar, Cours cours) {
		this.calendar = calendar;
		this.cours = cours;
	}

	/**
	 * getter de la date de debut
	 * @return
	 */
	public Calendar getDateDebut() {
		return this.calendar;
	}

	/**
	 * getter de cours
	 * @return
	 */
	public Cours getCours() {
		return this.cours;
	}

	/**
	 * setter de la date de but
	 * @param dateDebut
	 */
	public void setDateDebut(Calendar calendar) {
		this.calendar = calendar;
	}

	/**
	 * setter du cours
	 * @param cours
	 */
	public void setCours(Cours cours) {
		this.cours = cours;
	}
	
	
}
